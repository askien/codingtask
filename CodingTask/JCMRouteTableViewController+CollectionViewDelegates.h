#import <UIKit/UIKit.h>
#import "JCMRouteTableViewController.h"

@interface JCMRouteTableViewController (CollectionViewDelegates) <UICollectionViewDelegate, UICollectionViewDataSource>
@end
