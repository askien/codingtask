#import <Foundation/Foundation.h>

@interface JCMContact : NSObject

@property (nonatomic) NSString *name;
@property (nonatomic) NSString *phoneNumber;

@end
