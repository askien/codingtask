#import "JCMStop.h"

@implementation JCMStop

+ (NSArray *)stopsFromJSONArray:(NSArray *)parsedStops {
    if (![parsedStops isKindOfClass:[NSArray class]]) {
        return nil;
    }
    NSMutableArray *stops = [[NSMutableArray alloc] init];
    for (NSDictionary *parsedStop in parsedStops) {
        if (![parsedStop isKindOfClass:[NSDictionary class]]) {
            return nil;
        }
        JCMStop *stop = [JCMStop stopFromJSONDictionary:parsedStop];
        if (!stop) {
            return nil;
        }
        [stops addObject:stop];
    }
    return [stops copy];
}

+ (JCMStop *)stopFromJSONDictionary:(NSDictionary *)parsedStop {
    if (![parsedStop isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    JCMStop *stop = [[JCMStop alloc] init];
    NSString *dateTime = parsedStop[@"datetime"];
    if (![dateTime isKindOfClass:[NSString class]]) {
        return nil;
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    stop.date = [dateFormatter dateFromString:dateTime];
    
    NSString *name = parsedStop[@"name"];
    if (![name isKindOfClass:[NSString class]] && ![name isKindOfClass:[NSNull class]]) {
        return nil;
    }
    stop.name = [name isKindOfClass:[NSNull class]] ? nil : name;

    NSNumber *latitude = parsedStop[@"lat"];
    NSNumber *longitude = parsedStop[@"lng"];
    if (![latitude isKindOfClass:[NSNumber class]] || ![longitude isKindOfClass:[NSNumber class]]) {
        return nil;
    }
    CLLocationCoordinate2D coordinates = CLLocationCoordinate2DMake([latitude doubleValue], [longitude doubleValue]);
    stop.coordinates = coordinates;
    return stop;
}

- (NSString *)formattedDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    return [dateFormatter stringFromDate:self.date];
}

@end
