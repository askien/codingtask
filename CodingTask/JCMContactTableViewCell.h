#import <UIKit/UIKit.h>

@protocol JCMPhoneTappedDelegate;

@interface JCMContactTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIButton *phoneNumberButton;

@property (weak, nonatomic) id<JCMPhoneTappedDelegate>delegate;

- (IBAction)phoneNumberButtonTapped:(id)sender;

@end
