#import "JCMRoutesTableViewController+CollectionViewDelegates.h"
#import "JCMTransportationTypeCollectionViewCell.h"
#import "JCMSegment.h"
#import "JCMRoute.h"
#import "Hexcolor.h"

static NSString *reuseIdentifier = @"TransportationTypeCell";

@implementation JCMRoutesTableViewController (CollectionViewDelegates)

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    JCMRoute *route = self.routes[collectionView.tag];
    return [JCMSegment segmentsWithMovement:route.segments].count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    JCMTransportationTypeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    JCMRoute *route = self.routes[collectionView.tag];
    JCMSegment *segment = [JCMSegment segmentsWithMovement:route.segments][indexPath.row];
    UIColor *color = [UIColor colorWithHexString:segment.color];
    cell.containerView.backgroundColor = color;
    cell.name.text = segment.name;
    return cell;
}

@end