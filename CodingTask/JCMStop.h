#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface JCMStop : NSObject

@property (nonatomic) NSDate *date;
@property (nonatomic) NSString *name;
@property (nonatomic) CLLocationCoordinate2D coordinates;

+ (NSArray *)stopsFromJSONArray:(NSArray *)parsedStops;
- (NSString *)formattedDate;
@end
