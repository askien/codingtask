#import "JCMRouteTableViewController.h"
#import "JCMRoute.h"
#import "JCMSegment.h"
#import "JCMStop.h"
#import "JCMRouteTableViewCell.h"
#import "JCMStopTableViewCell.h"
#import "JCMSimpleSegmentTableViewCell.h"
#import "JCMSegmentTableViewCell.h"
#import "JCMContactTableViewCell.h"
#import "JCMContact.h"

@interface JCMRouteTableViewController ()

@property (nonatomic) NSArray *rows;

@end

@implementation JCMRouteTableViewController

- (void)setupRows {
    NSMutableArray *rows = [[NSMutableArray alloc] init];
    [rows addObject: self.route];
    for (JCMSegment *segment in self.route.segments) {
        if (segment.polyline) {
            [rows addObject:segment.stops.firstObject];
        }
        [rows addObject:segment];
    }
    JCMSegment *lastSegment = rows.lastObject;
    if ([lastSegment isKindOfClass:[JCMSegment class]] && lastSegment.polyline) {
        JCMStop *lastStop = lastSegment.stops.lastObject;
        [rows addObject:lastStop];
    }
    if ([self.route.transportationType isEqualToString:@"taxi"]) {
        [rows addObjectsFromArray:self.route.contacts];
    }
    self.rows = [rows copy];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"JCMRouteTableViewCell" bundle:nil] forCellReuseIdentifier:@"RouteCell"];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.navigationItem.title = NSLocalizedString(@"Route", @"Route title on route detail screen");
}

// Doing it on viewDidAppear because storyboard instanciates VC and calls viewDidLoad
// before I can set property
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self setupRows];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.rows.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id row = self.rows[indexPath.row];
    if ([row isKindOfClass:[JCMRoute class]]) {
        JCMRouteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RouteCell" forIndexPath:indexPath];
        [cell setCollectionViewDataSourceDelegate:(id<UICollectionViewDelegate, UICollectionViewDataSource>)self forRow:indexPath.row];
        JCMRoute *route = self.route;
        cell.contentView.backgroundColor = [UIColor whiteColor];
        cell.duration.text = [route formattedDuration];
        cell.times.text = [route formattedTimes];
        cell.price.text = [route formattedPrice];
        cell.transportationType.text = [route formattedTransportationType];
        return cell;
    }
    if ([row isKindOfClass:[JCMContact class]]) {
        JCMContact *contact = self.rows[indexPath.row];
        JCMContactTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactCell" forIndexPath:indexPath];
        cell.name.text = contact.name;
        [cell.phoneNumberButton setTitle:contact.phoneNumber forState:UIControlStateNormal];
        cell.delegate = self;
        return cell;
    }
    if ([row isKindOfClass:[JCMSegment class]]) {
        JCMSegment *segment = self.rows[indexPath.row];
        if ([@[@"walking",@"change", @"parking", @"setup"] containsObject:segment.travelMode]) {
            JCMSimpleSegmentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SimpleSegment" forIndexPath:indexPath];
            cell.text.text = [NSString stringWithFormat:@"%@ %@",[segment formattedDuration], segment.travelMode];
            return cell;
        }
        JCMSegmentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SegmentCell" forIndexPath:indexPath];
        cell.text.text = [segment formattedTravelMode];
        cell.time.text = [segment formattedDuration];
        if (segment.numberOfStops == 0) {
            cell.numberOfStops.hidden = YES;
        } else {
            cell.numberOfStops.hidden = NO;
            if (segment.numberOfStops == 1) {
                cell.numberOfStops.text = [NSString stringWithFormat:NSLocalizedString(@"%lu Stop",@"Number of stops. Singular case."),segment.numberOfStops];
            } else {
                cell.numberOfStops.text = [NSString stringWithFormat:NSLocalizedString(@"%lu Stops",@"Number of stops. Plural case Parameter is actual number."),segment.numberOfStops];
            }
        }
        return cell;
    }
    if ([row isKindOfClass:[JCMStop class]]) {
        JCMStopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StopCell" forIndexPath:indexPath];
        JCMStop *stop = self.rows[indexPath.row];
        cell.time.text = [stop formattedDate];
        cell.location.text = stop.name ?: NSLocalizedString(@"Your Location", @"Your Location, used on the route description");
        return cell;
    }
    return nil;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)didTapPhoneNumberWithURL:(NSURL *)URL {
    if ([[UIApplication sharedApplication] canOpenURL:URL]) {
        [[UIApplication sharedApplication] openURL:URL];
    }
}

@end
