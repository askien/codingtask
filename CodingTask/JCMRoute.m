#import "JCMRoute.h"
#import "JCMSegment.h"
#import "JCMStop.h"
#import "JCMContact.h"

@implementation JCMRoute

+ (NSArray *)requiredKeys {
    return @[@"price", @"type", @"provider", @"segments"];
}

+ (NSDictionary *)transportationTypes {
    return @{@"public_transport": NSLocalizedString(@"Public Transport", @"Public Transport transportation type"),
             @"car_sharing": [NSNull null],
             @"private_bike": NSLocalizedString(@"Cycle", @"Cycle transportation type"),
             @"bike_sharing": [NSNull null],
             @"taxi": NSLocalizedString(@"Taxi", @"Taxi transportation type")};
}

+ (NSDictionary *)providers {
    return @{@"car2go":@"car2Go",
             @"drivenow":@"DriveNow",
             @"nextbike":@"NextBike",
             @"callabike":@"Call a Bike"};
}

+ (NSData *)loadRoutesFromFileWithName:(NSString *)filename {
    NSData *fileData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[filename stringByDeletingPathExtension] ofType:[filename pathExtension]]];
    if (!fileData) {
        return nil;
    }
    return fileData;
}

+ (NSArray *)routesFromData:(NSData *)data {
    if (!data) {
        return nil;
    }
    NSError *error;
    NSDictionary *parsedJSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    if (![parsedJSON isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    if (![parsedJSON[@"routes"] isKindOfClass:[NSArray class]]) {
        return nil;
    }
    NSArray *parsedRoutes = parsedJSON[@"routes"];
    NSMutableArray *routes = [[NSMutableArray alloc] init];
    for (id parsedRoute in parsedRoutes) {
        if (![parsedRoute isKindOfClass:[NSDictionary class]]) {
            continue;
        }
        JCMRoute *route = [self routeFromJSONDictionary:parsedRoute];
        if (!route) {
            continue;
        }
        [routes addObject:route];
    }
    return [routes copy];
}

+ (NSArray *)routesFromFileWithName:(NSString *)filename {
    NSData *routeData = [self loadRoutesFromFileWithName:filename];
    if (!routeData) {
        return nil;
    }
    return [JCMRoute routesFromData:routeData];
}

+ (JCMRoute *)routeFromJSONDictionary:(NSDictionary *)parsedRoute {
    JCMRoute *route = [[JCMRoute alloc] init];
    
    NSDictionary *price = parsedRoute[@"price"];
    if (![price isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    if (price[@"amount"]) {
        NSInteger amount = [((NSNumber *)price[@"amount"]) integerValue];
        route.price = amount;
        NSString *currency = price[@"currency"];
        if (![currency isKindOfClass:[NSString class]]) {
            return nil;
        }
        route.currency = currency;
    }
    
    NSString *type = parsedRoute[@"type"];
    if (![type isKindOfClass:[NSString class]]) {
        return nil;
    }
    route.transportationType = type;
    
    NSString *provider = parsedRoute[@"provider"];
    if (![provider isKindOfClass:[NSString class]]) {
        return nil;
    }
    route.provider = provider;
    
    NSArray *parsedSegments = parsedRoute[@"segments"];
    if (![parsedSegments isKindOfClass:[NSArray class]]) {
        return nil;
    }

    NSArray *segments = [JCMSegment segmentsFromJSONArray:parsedSegments];
    if (!segments) {
        return nil;
    }
    route.segments = segments;
    
    NSDictionary *parsedProperties = parsedRoute[@"properties"];
    if (![parsedProperties isKindOfClass:[NSDictionary class]] && ![parsedProperties isKindOfClass:[NSNull class]]) {
        return nil;
    }
    if ([route.transportationType isEqualToString:@"taxi"]) {
        NSArray *contacts = [JCMRoute contactsFromJSONArray:parsedProperties[@"companies"]];
        route.contacts = contacts;
    }
    
    JCMSegment *firstSegment = segments.firstObject;
    JCMSegment *lastSegment = segments.lastObject;
    JCMStop *firstStop = firstSegment.stops.firstObject;
    JCMStop *lastStop = lastSegment.stops.lastObject;
    route.startDate = firstStop.date;
    route.endDate = lastStop.date;

    route.startLocation = firstStop.coordinates;
    route.endLocation = lastStop.coordinates;
    
    return route;
}

- (NSTimeInterval)duration {
    return [self.endDate timeIntervalSinceDate:self.startDate];
}

- (NSString *)formattedDuration {
    return [NSString stringWithFormat:@"%lu min",(unsigned long)self.duration/60];
}

- (NSString *)formattedPrice {
    NSDictionary *currencySymbolLookup = @{@"EUR":@"€"};
    NSString *approximateValue = [self.transportationType isEqualToString:@"taxi"] ? @"~ " : @"";
    NSString *currencySymbol = currencySymbolLookup[self.currency] ?: @"";
    return [NSString stringWithFormat:@"%@%@%.2f",approximateValue,currencySymbol,self.price/100.0];
}

- (NSString *)formattedTimes {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *formattedStartDate = [dateFormatter stringFromDate:self.startDate];
    NSString *formattedEndDate = [dateFormatter stringFromDate:self.endDate];
    return [NSString stringWithFormat:@"%@ → %@",formattedStartDate,formattedEndDate];
}

- (NSString *)formattedTransportationType {
    if ([JCMRoute transportationTypes][self.transportationType] == [NSNull null]) {
        return [JCMRoute providers][self.provider];
    }
    return [JCMRoute transportationTypes][self.transportationType];
}

+ (NSArray *)contactsFromJSONArray:(NSArray *)parsedProperties {
    NSMutableArray *contacts = [[NSMutableArray alloc] init];
    if (![parsedProperties isKindOfClass:[NSArray class]]) {
        return nil;
    }
    for (NSDictionary *parsedContact in parsedProperties) {
        if (![parsedContact isKindOfClass:[NSDictionary class]]) {
            continue;
        }
        NSString *name = parsedContact[@"name"];
        NSString *phone = parsedContact[@"phone"];
        if (![name isKindOfClass:[NSString class]] || ![name isKindOfClass:[NSString class]]) {
            continue;
        }
        JCMContact *contact = [[JCMContact alloc] init];
        contact.name = name;
        contact.phoneNumber = phone;
        [contacts addObject:contact];
    }
    return contacts.count > 0 ? [contacts copy] : nil;
}

@end
