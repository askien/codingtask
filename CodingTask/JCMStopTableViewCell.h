//
//  JCMStopTableViewCell.h
//  CodingTask
//
//  Created by Jorge Martins on 2/11/16.
//  Copyright © 2016 Jorge Martins. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JCMStopTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *location;

@end
