#import "JCMRoutesTableViewController.h"
#import "JCMRouteTableViewCell.h"
#import "JCMTransportationTypeCollectionViewCell.h"
#import "JCMRoute.h"
#import "JCMRouteTableViewController.h"
#import "JCMPullDownController.h"
#import "JCMMapViewController.h"

static NSString *reuseIdentifier = @"RouteCell";
static NSString *cellNibName = @"JCMRouteTableViewCell";
static NSString *routesFilename = @"routes.json";

@implementation JCMRoutesTableViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:cellNibName bundle:nil] forCellReuseIdentifier:reuseIdentifier];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.navigationItem.title = NSLocalizedString(@"Routes", @"Routes title on route-picking screen");
    self.routes = [JCMRoute routesFromFileWithName:routesFilename];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.routes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JCMRouteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
    [cell setCollectionViewDataSourceDelegate:(id<UICollectionViewDelegate, UICollectionViewDataSource>)self forRow:indexPath.row];
    JCMRoute *route = self.routes[indexPath.row];
    [self configureRouteCell:cell withRoute:route];
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger navigationBarHeight = 60;
    NSInteger defaultOffset = 44;
    JCMRoute *route = self.routes[indexPath.row];
    JCMRouteTableViewController *frontController = [self.storyboard instantiateViewControllerWithIdentifier:@
            "RouteTableViewController"];
    frontController.route = route;
    JCMMapViewController *backController = [self.storyboard instantiateViewControllerWithIdentifier:@
            "GoogleMaps"];
    backController.route = route;
    JCMPullDownController *pullDownController = [[JCMPullDownController alloc] initWithFrontController:frontController backController:backController];
    pullDownController.closedTopOffset = navigationBarHeight + defaultOffset;
    [self.navigationController pushViewController:pullDownController animated:NO];
}

#pragma mark - Helper methods

- (void)configureRouteCell:(JCMRouteTableViewCell *)cell withRoute:(JCMRoute *)route {
    cell.duration.text = [route formattedDuration];
    cell.times.text = [route formattedTimes];
    cell.price.text = [route formattedPrice];
    cell.transportationType.text = [route formattedTransportationType];
}

@end
