#import "JCMRouteTableViewCell.h"

@interface JCMRouteTableViewCell ()

@property (nonatomic) UIColor *selectedBackgroundColor;

@end

@implementation JCMRouteTableViewCell

- (void)awakeFromNib {
    self.selectedBackgroundColor = [UIColor colorWithRed:200.0/255.0 green: 200.0/255.0  blue:200.0/255.0  alpha: 1];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self setSelectedAndHighlighted:selected];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    [self setSelectedAndHighlighted:highlighted];
}

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource,UICollectionViewDelegate>)dataSourceDelegate forRow:(NSInteger)row {
    [self.transportationTypes registerNib:[UINib nibWithNibName:@"JCMTransportationTypeCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"TransportationTypeCell"];
    self.transportationTypes.delegate = dataSourceDelegate;
    self.transportationTypes.dataSource = dataSourceDelegate;
    self.transportationTypes.tag = row;
    [self.transportationTypes reloadData];
}

- (void)setSelectedAndHighlighted:(BOOL)highlighted {
    if (highlighted) {
        self.containerView.backgroundColor = self.selectedBackgroundColor;
        self.transportationTypes.backgroundColor = self.selectedBackgroundColor;
    } else {
        self.containerView.backgroundColor = [UIColor whiteColor];
        self.transportationTypes.backgroundColor = [UIColor whiteColor];
    }
}

@end
