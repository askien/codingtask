//
//  JCMSimpleSegmentTableViewCell.h
//  CodingTask
//
//  Created by Jorge Martins on 2/11/16.
//  Copyright © 2016 Jorge Martins. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JCMSimpleSegmentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *text;

@end
