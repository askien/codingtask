# Coding Task

To run the project, first install CocoaPods

`sudo gem install cocoapods`

Then run `pod install` on the project directory.

Open the workspace (not the project) and run.

Notes:

* Some elements have accessibility layers, but not all
* Didn't find a SVG library (yet) that I like, so no icons are displayed
* No tests (yet)
* Project doesn't really support anything other than Euros
* Transportation types are hard-coded. It would be nice to get updates from a service.
* No caching
