//
//  JCMStopTableViewCell.m
//  CodingTask
//
//  Created by Jorge Martins on 2/11/16.
//  Copyright © 2016 Jorge Martins. All rights reserved.
//

#import "JCMStopTableViewCell.h"

@implementation JCMStopTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
