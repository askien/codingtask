#import <UIKit/UIKit.h>

@interface JCMSegmentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *numberOfStops;
@property (weak, nonatomic) IBOutlet UILabel *text;

@end
