#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "JCMRoute.h"

@class GMSMapView;
@protocol GMSMapViewDelegate;

@interface JCMMapViewController : UIViewController <GMSMapViewDelegate>

@property (strong, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) JCMRoute *route;

@end
