#import <UIKit/UIKit.h>

@class JCMRoute;

@protocol JCMPhoneTappedDelegate <NSObject>

- (void)didTapPhoneNumberWithURL:(NSURL *)URL;

@end

@interface JCMRouteTableViewController : UITableViewController <JCMPhoneTappedDelegate>

@property (nonatomic) JCMRoute *route;

@end
