#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface JCMRoute : NSObject

@property (nonatomic) NSInteger price;
@property (nonatomic) NSString *currency;
@property (nonatomic) NSDate *startDate;
@property (nonatomic) NSDate *endDate;
@property (nonatomic) CLLocationCoordinate2D startLocation;
@property (nonatomic) CLLocationCoordinate2D endLocation;
@property (nonatomic) NSArray *segments;
@property (nonatomic) NSArray *contacts;
@property (nonatomic) NSString *transportationType;
@property (nonatomic) NSString *provider;

+ (NSArray *)routesFromFileWithName:(NSString *)filename;
+ (NSArray *)routesFromData:(NSData *)data;

- (NSTimeInterval)duration;

- (NSString *)formattedDuration;
- (NSString *)formattedPrice;
- (NSString *)formattedTimes;
- (NSString *)formattedTransportationType;

@end
