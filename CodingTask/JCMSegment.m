#import "JCMSegment.h"
#import "JCMStop.h"

@implementation JCMSegment

+ (NSArray *)segmentsFromJSONArray:(NSArray *)parsedSegments {
    if (![parsedSegments isKindOfClass:[NSArray class]]) {
        return nil;
    }
    NSMutableArray *segments = [[NSMutableArray alloc] init];
    for (NSDictionary *parsedSegment in parsedSegments) {
        if (![parsedSegment isKindOfClass:[NSDictionary class]]) {
            return nil;
        }
        JCMSegment *segment = [JCMSegment segmentFromJSONDictionary:parsedSegment];
        if (!segment) {
            return nil;
        }
        [segments addObject:segment];
    }
    return [segments copy];
}

+ (JCMSegment *)segmentFromJSONDictionary:(NSDictionary *)parsedSegment {
    JCMSegment *segment = [[JCMSegment alloc] init];
    NSArray *parsedStops = parsedSegment[@"stops"];
    if (![parsedStops isKindOfClass:[NSArray class]]) {
        return nil;
    }
    NSArray *stops = [JCMStop stopsFromJSONArray:parsedStops];
    if (!stops) {
        return nil;
    }
    segment.stops = stops;
    
    NSString *color = parsedSegment[@"color"];
    if (![color isKindOfClass:[NSString class]]) {
        return nil;
    }
    segment.color = color;

    NSString *polyline = parsedSegment[@"polyline"];
    if (![polyline isKindOfClass:[NSString class]] && ![polyline isKindOfClass:[NSNull class]]) {
        return nil;
    }
    segment.polyline = [polyline isKindOfClass:[NSNull class]] ? nil : polyline;
    
    NSString *travelMode = parsedSegment[@"travel_mode"];
    if (![travelMode isKindOfClass:[NSString class]]) {
        return nil;
    }
    segment.travelMode = travelMode;

    NSString *iconURL = parsedSegment[@"icon_url"];
    if (![iconURL isKindOfClass:[NSString class]]) {
        return nil;
    }
    segment.iconURL = iconURL;

    NSString *name = parsedSegment[@"name"];
    if (![name isKindOfClass:[NSString class]] && ![name isKindOfClass:[NSNull class]]) {
        return nil;
    }
    segment.name = [name isKindOfClass:[NSNull class]] ? nil : name;

    NSString *destination = parsedSegment[@"description"];
    if (![destination isKindOfClass:[NSString class]] && ![destination isKindOfClass:[NSNull class]]) {
        return nil;
    }
    segment.destination = [destination isKindOfClass:[NSNull class]] ? nil : destination;
    
    NSInteger numberOfStops = [((NSNumber *)parsedSegment[@"num_stops"]) integerValue];
    segment.numberOfStops = numberOfStops;

    return segment;
}

+ (NSArray *)segmentsWithMovement:(NSArray *)segments {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"polyline != nil"];
    return [segments filteredArrayUsingPredicate:predicate];
}

- (NSString *)formattedDuration {
    JCMStop *firstStop = self.stops.firstObject;
    JCMStop *lastStop = self.stops.lastObject;
    NSTimeInterval duration = [lastStop.date timeIntervalSinceDate:firstStop.date];
    return [NSString stringWithFormat:@"%lu min",(unsigned long)duration/60];
}

- (NSString *)formattedTravelMode {
    NSDictionary *lookupTable = @{@"cycling": NSLocalizedString(@"Cycle", @"Cycle as a segment on individual route"),
                                  @"walking": NSLocalizedString(@"Walk", @"Walk as a segment on individual route"),
                                  @"driving": NSLocalizedString(@"Drive", @"Drive as a segment on individual route")};
    NSString *formattedTravelMode = lookupTable[self.travelMode] ?: self.travelMode;
    if ([@[@"bus",@"subway"] containsObject:self.travelMode]) {
        formattedTravelMode = [NSString stringWithFormat:@"%@ → %@",self.name, self.destination];
    }
    return formattedTravelMode;
}

@end
