#import "JCMMapViewController.h"
#import "JCMRoute.h"
#import "JCMSegment.h"
#import "HexColor.h"

@implementation JCMMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:self.route.startLocation coordinate:self.route.endLocation];
    // This doesn't handle complicated paths very well
    [self.mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:15.0f]];
    for (JCMSegment *segment in self.route.segments) {
        if (segment.polyline) {
            GMSPath *path = [GMSPath pathFromEncodedPath:segment.polyline];
            GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
            polyline.strokeColor = [UIColor colorWithHexString:segment.color];
            polyline.strokeWidth = 5;
            polyline.map = self.mapView;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
