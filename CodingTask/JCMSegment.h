#import <Foundation/Foundation.h>

@interface JCMSegment : NSObject

@property (nonatomic) NSArray *stops;
@property (nonatomic) NSString *color;
@property (nonatomic) NSString *polyline;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *destination;
@property (nonatomic) NSString *travelMode;
@property (nonatomic) NSString *iconURL;
@property (nonatomic) NSInteger numberOfStops;

+ (NSArray *)segmentsFromJSONArray:(NSArray *)parsedSegments;
+ (NSArray *)segmentsWithMovement:(NSArray *)segments;

- (NSString *)formattedDuration;
- (NSString *)formattedTravelMode;

@end
