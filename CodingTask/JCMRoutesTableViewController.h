#import <UIKit/UIKit.h>

@interface JCMRoutesTableViewController : UITableViewController

@property (nonatomic) NSArray *routes;

@end
