#import <UIKit/UIKit.h>
#import "JCMRoutesTableViewController.h"

@interface JCMRoutesTableViewController (CollectionViewDelegates) <UICollectionViewDelegate, UICollectionViewDataSource>

@end
