#import "JCMContactTableViewCell.h"
#import "JCMRouteTableViewController.h"

@implementation JCMContactTableViewCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (IBAction)phoneNumberButtonTapped:(id)sender {
    [self.delegate didTapPhoneNumberWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",[self.phoneNumberButton titleForState:UIControlStateNormal]]]];
}
@end
