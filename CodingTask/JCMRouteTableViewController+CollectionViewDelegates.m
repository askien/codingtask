#import "JCMRouteTableViewController+CollectionViewDelegates.h"
#import "JCMTransportationTypeCollectionViewCell.h"
#import "JCMSegment.h"
#import "JCMRoute.h"
#import "Hexcolor.h"

@implementation JCMRouteTableViewController (CollectionViewDelegates)

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    JCMRoute *route = self.route;
    return [JCMSegment segmentsWithMovement:route.segments].count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    JCMTransportationTypeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TransportationTypeCell" forIndexPath:indexPath];
    JCMRoute *route = self.route;
    JCMSegment *segment = [JCMSegment segmentsWithMovement:route.segments][indexPath.row];
    UIColor *color = [UIColor colorWithHexString:segment.color];
    cell.containerView.backgroundColor = color;
    cell.name.text = segment.name;
    
    return cell;
}

@end