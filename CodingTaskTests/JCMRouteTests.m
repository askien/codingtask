#import <XCTest/XCTest.h>
#import "JCMRoute.h"

static NSString *sampleRoutesFilename;

@interface JCMRouteTests : XCTestCase

@property (nonatomic) NSString *sampleRoutesFilename;

@end

@implementation JCMRouteTests

+ (void)setUp {
    [super setUp];
    sampleRoutesFilename = [[NSBundle bundleForClass:[self class]] pathForResource:@"sampleroutes" ofType:@"json"];
}

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testRoutesFromDataReturnsCorrectNumberOfRoutes {
    NSData *data = [NSData dataWithContentsOfFile:sampleRoutesFilename];
    NSArray *routes = [JCMRoute routesFromData:data];
    XCTAssertEqual(8L, routes.count);
}

- (void)testRoutesFromDataReturnsCorrectNumberOfSegments {
    NSData *data = [NSData dataWithContentsOfFile:sampleRoutesFilename];
    NSArray *routes = [JCMRoute routesFromData:data];
    JCMRoute *route = routes[2];
    XCTAssertEqual(2L, route.segments.count);
}

- (void)testRoutesFromDataReturnsCorrectNumberOfContacts {
    NSData *data = [NSData dataWithContentsOfFile:sampleRoutesFilename];
    NSArray *routes = [JCMRoute routesFromData:data];
    JCMRoute *route = routes[7];
    XCTAssertEqual(5L, route.contacts.count);
}

@end
